package primjer.prvi;

public class Car {

    String year, brand, color;

    public Car(String y, String b, String c) {
        year = y;
        brand = b;
        color = c;

    }

    public Car() {

    }

    //Vracanje podatka o godini
    public String getYear() {
        return year;
    }

    //Postavljanje podatka o godini
    public void setYear(String year) {
        this.year = year;
    }

    //Vracanje podatka o marki
    public String getBrand() {
        return brand;
    }

    //Postavljanje podatka o marki
    public void setBrand(String brand) {
        this.brand = brand;
    }

    //Vracanje podatka o boji
    public String getColor() {
        return color;
    }

    //Postavljanje podatka o boji
    public void setColor(String color) {
        this.color = color;
    }

    //Metoda za formatirano ispisivanje podataka o autu
    public void printData() {
        System.out.println("Brand is " + brand + ". Year is " + year + " and color is " + color + ".");
    }
}
