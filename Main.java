package primjer.prvi;

public class Main {

    public static void main(String[] args) {
        //Instanciranje objekta klase Car
        Car golf = new Car("2012", "Volkswagen", "white");
        System.out.println("Brand is: " + golf.getBrand());

        //Instanciranje objekta klase Car defaultnim konstruktorom
        Car car = new Car();
        car.setBrand("Opel");
        car.setColor("gray");
        car.setYear("2005");

        //Pozivanje metode za printanje nad objektom golf
        golf.printData();
    }
}
